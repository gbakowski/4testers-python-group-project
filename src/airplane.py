class Airplane:
    def __init__(self, brand, total_able_seats):
        self.brand = brand
        self.total_mileage = 0
        self.total_available_seats = total_able_seats
        self.occupied_seats = 0

    def fly(self, distance):
        self.total_mileage += distance

    def is_service_required(self):
        if self.total_mileage > 10000:
            return True
        else:
            return False
#
    def board_passengers(self, number_of_passengers):
        #       if self.get_available_seats() < number_of_passengers:
        if self.total_available_seats - self.occupied_seats < number_of_passengers:
            overloaded_passengers = number_of_passengers - self.total_available_seats - self.occupied_seats
            self.occupied_seats = self.total_available_seats
            print(
                f"All {self.occupied_seats} seats are taken! There is no place for {overloaded_passengers} passengers!")
        else:
            self.occupied_seats += number_of_passengers

    def get_available_seats(self):
        return self.total_available_seats - self.occupied_seats


if __name__ == '__main__':
    airplane1 = Airplane("Boeing 575", 200)
    airplane2 = Airplane("Airbus 2000", 300)

    airplane1.fly(1000)
    airplane2.fly(20000)

    print(airplane1.total_mileage)
    print(airplane2.total_mileage)

    print(airplane1.is_service_required())
    print(airplane2.is_service_required())

    airplane1.board_passengers(100)
    airplane2.board_passengers(301)

    print(f"Total occupied seats in the first airplane: {airplane1.occupied_seats}.")
    print(f"Total occupied seats in the second airplane: {airplane2.occupied_seats}.")

    print(f"Available seats in the first airplane: {airplane1.get_available_seats()}.")
    print(f"Available seats in the second airplane: {airplane2.get_available_seats()}.")

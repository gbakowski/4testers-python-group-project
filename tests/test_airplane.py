from src.airplane import Airplane
#testy

def test_initial_state_of_airplane():
    airplane = Airplane("Boeing 575", 200)
    assert airplane.brand == "Boeing 575"
    assert airplane.total_available_seats == 200
    assert airplane.total_mileage == 0
    assert airplane.occupied_seats == 0


def test_flied_correct_distance_once():
    airplane = Airplane("Boeing 575", 200)
    airplane.fly(5000)
    assert airplane.total_mileage == 5000


def test_flied_correct_distance_twice():
    airplane = Airplane("Boeing 575", 200)
    airplane.fly(5000)
    airplane.fly(3000)
    assert airplane.total_mileage == 8000


def test_airplane_service_is_not_required():
    airplane = Airplane("Boeing 575", 200)
    airplane.fly(9999)
    assert not airplane.is_service_required()


def test_airplane_service_is_required():
    airplane = Airplane("Boeing 575", 200)
    airplane.fly(10001)
    assert airplane.is_service_required()


def test_airplane_board_passengers():
    airplane = Airplane("Boeing 575", 200)
    airplane.board_passengers(180)
    assert airplane.get_available_seats() == 20


def test_airplane_board_passangers_is_overloaded():
    airplane = Airplane("Boeing 575", 200)
    airplane.board_passengers(201)
    assert airplane.get_available_seats() == 0
    assert airplane.occupied_seats == 200
